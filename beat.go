package main

import (
	"fmt"
	"time"
)

const (
	SECONDS_PER_HOUR = 3600
	SECONDS_PER_MIN  = 60
	SECONDS_PER_DAY  = 86400
	BEATS_PER_SECOND = 1000
	BEATS_PER_DAY    = SECONDS_PER_DAY / BEATS_PER_SECOND
)

// getInternetTime returns the current Swatch Internet Time (aka Beat Time).
// It calculates the time based on the timezone of Biel, Switzerland (CET),
// which is UTC+1, without considering Daylight Saving Time adjustments.
func getInternetTime() int {
	// Get the current time in UTC
	nowUTC := time.Now().UTC()

	// Define the location of Biel, Switzerland, as per CET (UTC+1)
	loc, err := time.LoadLocation("CET")
	if err != nil { // Handle error here!
		return -1 // Return a sentinel value to indicate failure
	}

	// Convert the current UTC time to Biel, Switzerland's local time
	nowBiel := nowUTC.In(loc)

	// Calculate the total seconds since midnight
	secondsSinceMidnight := nowBiel.Hour()*SECONDS_PER_HOUR + nowBiel.Minute()*SECONDS_PER_MIN + nowBiel.Second()

	// Convert seconds to beats (1 beat = 86.4 seconds, hence 86400 seconds in a day / 1000 beats)
	beats := (secondsSinceMidnight * BEATS_PER_SECOND) / SECONDS_PER_DAY

	return beats
}

func main() {
	beats := getInternetTime()
	fmt.Printf("Current Swatch Internet Time: @%d\n", beats)
}
