# Beat (golang)

A go cli to print [Swatch Internet Time](https://en.wikipedia.org/wiki/Swatch_Internet_Time) on the terminal because, why not?

## To run

```
go run beat.go
```

## To build and install

- `go build beat.go`
- `mv ./beat ~/bin` (or wherever in your $PATH)
